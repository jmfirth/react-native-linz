import React, { Component } from 'react';
import App from './src'
import { AppRegistry } from 'react-native';


AppRegistry.registerComponent('HomeAssistant', () => App);
AppRegistry.runApplication('HomeAssistant', {
  rootTag: document.getElementById('react-root')
});
