import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  titleContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {

  },
  logoContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red'
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 7,
    padding: 15
  },
  buttonText: {
    color: 'blue'
  }
})


class Home extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Finance Tracker</Text>
        </View>
        <View style={styles.logoContainer}>
          <Text>Logo</Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={Actions.Home}>
            <Text style={styles.buttonText}>Track</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}


const mapStateToProps = state => ({
  Modules: {
    Home: state.Modules.Home
  }
})


export default connect(mapStateToProps)(Home)
