import { UPDATE_HOME } from './constants'


export const updateHome = (home = {}) => ({
  type: UPDATE_HOME,
  home
})
