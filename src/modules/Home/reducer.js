import { handleActions } from 'redux-actions'
import { UPDATE_HOME } from './constants'


const initialState = {
  timesLoaded: 0
}


export default handleActions({
  [UPDATE_HOME]: (state, action) => ({ ...state, ...action.home })
}, initialState)
