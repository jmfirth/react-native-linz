import { NAME } from './constants'
import Container from './Home'
import * as actions from './actions'
import reducer from './reducer'


export default {
  NAME,
  Container,
  actions,
  reducer
}
