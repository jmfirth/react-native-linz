import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StyleSheet, View, Text } from 'react-native'


const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})


class Home extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Hello, World!</Text>
      </View>
    )
  }
}


const mapStateToProps = state => ({
  Modules: {
    Home: state.Modules.Home
  }
})


export default connect(mapStateToProps)(Home)
