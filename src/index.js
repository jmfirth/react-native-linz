import React from 'react'
import { Platform, AsyncStorage } from 'react-native'
import { Provider, connect } from 'react-redux'
import { persistStore } from 'redux-persist'
import { Scene, Modal, Router, ActionConst } from 'react-native-router-flux'
import { Home, Editor } from './modules'
import createStore from './createStore'


const RouterWithRedux = connect()(Router)


const App = () => {
  const store = createStore()

  /*
  const persistor = (async function() {
    return await persistStore(store, { storage: Platform.OS !== 'web' ? AsyncStorage : undefined })
  })()
  */

  return (
    <Provider store={store}>
    {/* <Provider store={store} persistor={persistor}> */}
      <RouterWithRedux>
        <Scene key="modal" component={Modal}>
          <Scene key="app" hideNavBar>
            <Scene
              key={Home.NAME}
              component={Home.Container}
              title={Home.NAME}
              type={ActionConst.REPLACE}
              initial
            />
          </Scene>
        </Scene>
      </RouterWithRedux>
    </Provider>
  )
}


export default App
