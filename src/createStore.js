import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { autoRehydrate } from 'redux-persist'
import { Home } from './modules'


const middleware = compose(applyMiddleware(thunk), autoRehydrate());


export default (data = {}) => {
  const rootReducer = combineReducers({
    Modules: combineReducers({
      [Home.NAME]: Home.reducer
    })
  })

  return createStore(rootReducer, undefined, middleware)
}
