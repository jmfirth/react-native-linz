export const theme = {
  backgroundColor: '#212121',
  canvasColor: '#444',
  primaryColor: 'red',
  accentColor: 'purple',
  primaryTextColor: '#fefefe',
  secondaryTextColor: '#ccc'
}
