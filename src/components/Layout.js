import React from 'react'
import { StyleSheet, View, Text } from 'react-native'


const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#222'
  },
  headerContainer: {
    position: 'absolute',
    top: 20,
    left: 0,
    right: 0,
    height: 40
  },
  header: {
    flex: 1,
    flexDirection: 'row'
  },
  iconContainer: {
    height: 40,
    width: 40,
    marginLeft: 10,
    marginRight: 10,
    padding: 5
  },
  title: {
    height: 40,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleText: {
    color: '#fff'
  },
  contentContainer: {
    position: 'absolute',
    top: 40,
    left: 0,
    right: 0,
    bottom: 0
  }
})


const Layout = ({
  iconLeft = null,
  title = null,
  iconRight = null,
  hasHeader = !!(iconLeft || title || iconRight),
  children
}) => (
  <View style={styles.container}>
    {hasHeader &&
      <View style={styles.headerContainer}>
        <View style={styles.header}>
          <View style={styles.iconContainer}>
            {iconLeft}
          </View>
          <View style={styles.title}>
            {title && <Text style={styles.titleText}>{title}</Text>}
          </View>
          <View style={styles.iconContainer}>
            {iconRight}
          </View>
        </View>
      </View>
    }
    <View style={StyleSheet.flatten([styles.contentContainer, { top: hasHeader ? 40 : 0 }])}>
      {children}
    </View>
  </View>
)


export default Layout
